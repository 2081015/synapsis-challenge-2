package config

import "gitlab.com/2081015/synapsis-challenge-2/utils"

var (

	//db
	host     = utils.GetEnv("DB_HOST")
	port     = utils.GetEnv("DB_PORT")
	user     = utils.GetEnv("DB_USERNAME")
	password = utils.GetEnv("DB_PASSWORD")
	dbName   = utils.GetEnv("DB_NAME")
	minConns = utils.GetEnv("DB_POOL_MIN")
	maxConns = utils.GetEnv("DB_POOL_MAX")

	//host
	ServerHost     = utils.GetEnv("GRPC_SERVER_URI")
	ServerPort     = utils.GetEnv("GRPC_SERVER_PORT")
	EndpointPrefix = utils.GetEnv("ENDPOINT_PREFIX")
	DefaultLimit   = utils.GetEnv("DEFAULT_LIMIT")
)
