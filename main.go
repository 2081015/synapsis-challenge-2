package main

import (
	"log"
	"net"
	"time"

	"gitlab.com/2081015/synapsis-challenge-2/config"
	"gitlab.com/2081015/synapsis-challenge-2/proto/pb"
	"gitlab.com/2081015/synapsis-challenge-2/repository"
	"gitlab.com/2081015/synapsis-challenge-2/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	listener, err := net.Listen("tcp", ":"+config.ServerPort)
	if err != nil {
		log.Fatalf("cannot listen: %v", err)
	}

	time.Local = time.UTC
	db := config.NewPostgresPool()

	profileStore := repository.NewProfileStore(db)
	profileRepo := repository.NewProfileRepository(profileStore)
	profileService := service.NewProfileService(profileRepo)

	gServer := grpc.NewServer()
	pb.RegisterUserProfileServer(gServer, profileService)
	reflection.Register(gServer)

	if err := gServer.Serve(listener); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
