package domain

import (
	"gitlab.com/2081015/synapsis-challenge-2/model/web"
	"gitlab.com/2081015/synapsis-challenge-2/proto/pb"
)

type User struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Phone    string `json:"phone"`
}

func ConvertReqToUserObj(req *pb.UserRequest) User {
	return User{ID: int(*req.Id), Name: req.Name, Email: req.Email, Password: req.Password, Phone: req.Phone}
}

func ConvertRequestToWebResponse(user User, wr web.WebResponse) *pb.WebResponse {
	code, status, message, userID := new(uint32), new(bool), new(string), new(uint32)
	*code, *status, *message, *userID = uint32(wr.Code), wr.Status, wr.Message, uint32(user.ID)

	return &pb.WebResponse{
		Code:         code,
		Status:       true,
		Message:      *message,
		UserResponse: &pb.UserResponse{Id: userID, Name: user.Name, Email: user.Email, Password: user.Password, Phone: user.Phone},
	}
}

func ConvertRequestUpdatePassword(req *pb.UserUpdatePasswordRequest) User {

	userID := int(req.GetId())

	return User{ID: userID, Password: req.ConfirmPass}

}
