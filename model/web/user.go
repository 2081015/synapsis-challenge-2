package web

type UserCreateRequest struct {
	Name     string `json:"name" validate:"required"`
	Email    string `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
	Phone    string `json:"phone" validate:"required"`
}

type UserUpdatePasswordRequest struct {
	Id          *int   `json:"id"`
	Name        string `json:"name" validate:"required"`
	OldPassword string `json:"old_password" validate:"required"`
	Password    string `json:"password" validate:"required"`
	ConfirmPass string `json:"confirm_password" validate:"required"`
	UpdatePass  bool   `json:"update_pass" validate:"required"`
}

type UserResponse struct {
	Id       *int   `json:"id"`
	Name     string `json:"name"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Phone    string `json:"phone"`
}
