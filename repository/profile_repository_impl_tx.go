package repository

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5"
	"gitlab.com/2081015/synapsis-challenge-2/model/domain"
)

type ProfileRepositoryImpl struct {
	DB Store
}

type ProfileRepository interface {
	GetUserProfileByIDTx(ctx context.Context, ID int) (domain.User, error)
	CreateProfileByIDTx(ctx context.Context, user domain.User) error
	UpdateProfileByIDTx(ctx context.Context, user domain.User, updatePass bool) error
	DeleteProfileByIDTx(ctx context.Context, ID int) error
}

func NewProfileRepository(db Store) ProfileRepository {
	return &ProfileRepositoryImpl{
		DB: db,
	}
}

func (pri *ProfileRepositoryImpl) GetUserProfileByIDTx(ctx context.Context, ID int) (domain.User, error) {

	var data domain.User
	var err error

	err = pri.DB.WithTransaction(ctx, func(tx pgx.Tx) error {
		data, err = pri.GetUserProfileByID(ctx, tx, ID)
		if err != nil {
			return err
		}

		return nil
	})

	if err != nil {
		return domain.User{}, err
	}

	return data, err

}

func (pri *ProfileRepositoryImpl) CreateProfileByIDTx(ctx context.Context, user domain.User) error {

	var err error

	err = pri.DB.WithTransaction(ctx, func(tx pgx.Tx) error {
		err = pri.CreateProfileByID(ctx, tx, user)
		if err != nil {
			return err
		}

		return nil

	})

	if err != nil {
		return err
	}

	return nil

}

func (pri *ProfileRepositoryImpl) UpdateProfileByIDTx(ctx context.Context, user domain.User, updatePass bool) error {

	var err error

	err = pri.DB.WithTransaction(ctx, func(tx pgx.Tx) error {
		err = pri.UpdateProfileByID(ctx, tx, user, updatePass)
		if err != nil {
			return err
		}
		return nil
	})

	if err != nil {
		return err
	}

	return nil
}

func (pri *ProfileRepositoryImpl) DeleteProfileByIDTx(ctx context.Context, ID int) error {

	var err error

	err = pri.DB.WithTransaction(ctx, func(tx pgx.Tx) error {
		err = pri.DeleteProfileByID(ctx, tx, ID)
		if err != nil {
			fmt.Println(err)
			return err
		}

		return nil
	})

	if err != nil {
		fmt.Println(err)
		return err
	}

	return nil
}
