package repository

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5"
	"gitlab.com/2081015/synapsis-challenge-2/model/domain"
)

func (repository *ProfileRepositoryImpl) GetUserProfileByID(ctx context.Context, db pgx.Tx, ID int) (domain.User, error) {
	queryStr := "SELECT * FROM users WHERE id = $1"

	row, err := db.Query(context.Background(), queryStr, ID)

	if err != nil {
		return domain.User{}, err
	}

	defer row.Close()

	data, err := pgx.CollectOneRow(row, pgx.RowToStructByPos[domain.User])

	if err != nil {
		return domain.User{}, err
	}

	return data, nil

}

func (repository *ProfileRepositoryImpl) CreateProfileByID(ctx context.Context, db pgx.Tx, user domain.User) error {
	dmlStr := `
	INSERT INTO users (name, email, password, phone) 
	VALUES ($1, $2, $3, $4);`
	_, err := db.Exec(ctx, dmlStr, user.Name, user.Email, user.Password, user.Phone)
	if err != nil {
		fmt.Println("query error : ", err)
		return err
	}

	return nil

}

func (repository *ProfileRepositoryImpl) UpdateProfileByID(ctx context.Context, db pgx.Tx, user domain.User, updatePass bool) error {

	var dmlStr string

	if updatePass {
		fmt.Print(user.Password)

		dmlStr = `
		UPDATE users 
		SET password = $2
		WHERE id = $1;
		`

		_, err := db.Exec(ctx, dmlStr, user.ID, user.Password)
		if err != nil {
			fmt.Println("query error : ", err)
			return err
		}

		return nil
	}

	dmlStr = `
	UPDATE users 
	SET name = $2, email = $3, phone = $4 
	WHERE id = $1;
	`

	_, err := db.Exec(ctx, dmlStr, user.ID, user.Name, user.Email, user.Phone)
	if err != nil {
		fmt.Println("query error : ", err)
		return err
	}

	return nil
}

func (repository *ProfileRepositoryImpl) DeleteProfileByID(ctx context.Context, db pgx.Tx, ID int) error {
	dmlStr := `
	DELETE FROM users 
	WHERE id = $1
	`

	_, err := db.Exec(ctx, dmlStr, ID)
	if err != nil {
		fmt.Println("query error : ", err)
		return err
	}
	return nil

}
