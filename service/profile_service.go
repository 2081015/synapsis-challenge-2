package service

import (
	"context"
	"fmt"

	"gitlab.com/2081015/synapsis-challenge-2/model/domain"
	"gitlab.com/2081015/synapsis-challenge-2/model/web"
	"gitlab.com/2081015/synapsis-challenge-2/proto/pb"
	"gitlab.com/2081015/synapsis-challenge-2/repository"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// type ProfileServiceInterface interface{}

type ProfileService struct {
	pb.UnimplementedUserProfileServer
	profileRepo repository.ProfileRepository
}

func NewProfileService(profileRepo repository.ProfileRepository) *ProfileService {
	return &ProfileService{
		profileRepo: profileRepo,
	}
}

func (ps *ProfileService) GetUserProfileByID(ctx context.Context, req *pb.UserRequest) (*pb.WebResponse, error) {

	if user, err := ps.profileRepo.GetUserProfileByIDTx(ctx, int(req.GetId())); err != nil {

		webResponseObj := web.WebResponse{
			Code:    500,
			Status:  false,
			Message: "fail to get profile by user ID",
		}

		webResponse := domain.ConvertRequestToWebResponse(user, webResponseObj)
		return webResponse, err

	} else {

		webResponseObj := web.WebResponse{
			Code:    200,
			Status:  true,
			Message: "success to get profile by user ID",
		}

		webResponse := domain.ConvertRequestToWebResponse(user, webResponseObj)
		return webResponse, err
	}

}

func (ps *ProfileService) CreateProfileByID(ctx context.Context, req *pb.UserRequest) (*pb.WebResponse, error) {

	userReq := domain.ConvertReqToUserObj(req)

	if err := ps.profileRepo.CreateProfileByIDTx(ctx, userReq); err != nil {

		webResponseObj := web.WebResponse{
			Code:    500,
			Status:  false,
			Message: "fail to create profile by user ID",
		}

		webResponse := domain.ConvertRequestToWebResponse(domain.User{}, webResponseObj)
		return webResponse, err
	}

	webResponseObj := web.WebResponse{
		Code:    200,
		Status:  true,
		Message: "success to create profile by user ID",
	}

	webResponse := domain.ConvertRequestToWebResponse(domain.User{}, webResponseObj)
	return webResponse, nil

}

func (ps *ProfileService) UpdateProfileByID(ctx context.Context, req *pb.UserRequest) (*pb.WebResponse, error) {

	userReq := domain.ConvertReqToUserObj(req)

	if err := ps.profileRepo.UpdateProfileByIDTx(ctx, userReq, false); err != nil {

		webResponseObj := web.WebResponse{
			Code:    500,
			Status:  false,
			Message: "fail to update profile by user ID",
		}

		webResponse := domain.ConvertRequestToWebResponse(domain.User{}, webResponseObj)
		return webResponse, err
	}

	webResponseObj := web.WebResponse{
		Code:    200,
		Status:  true,
		Message: "success to update profile by user ID",
	}

	webResponse := domain.ConvertRequestToWebResponse(domain.User{}, webResponseObj)
	return webResponse, nil

}

func (ps *ProfileService) DeleteProfileByID(ctx context.Context, req *pb.UserRequest) (*pb.WebResponse, error) {

	if err := ps.profileRepo.DeleteProfileByIDTx(ctx, int(req.GetId())); err != nil {
		fmt.Println(err)
		webResponseObj := web.WebResponse{
			Code:    500,
			Status:  false,
			Message: "fail to delete profile by user ID",
		}

		webResponse := domain.ConvertRequestToWebResponse(domain.User{}, webResponseObj)
		return nil, status.Errorf(codes.Code(*webResponse.Code), webResponse.Message)
	}

	webResponseObj := web.WebResponse{
		Code:    200,
		Status:  true,
		Message: "success to delete profile by user ID",
	}

	webResponse := domain.ConvertRequestToWebResponse(domain.User{}, webResponseObj)
	return webResponse, nil
}

func (ps *ProfileService) ChangePasswordByID(ctx context.Context, req *pb.UserUpdatePasswordRequest) (*pb.WebResponse, error) {

	user, err := ps.profileRepo.GetUserProfileByIDTx(ctx, int(req.GetId()))

	if err != nil {

		webResponseObj := web.WebResponse{
			Code:    500,
			Status:  false,
			Message: "fail to get profile by user ID",
		}

		webResponse := domain.ConvertRequestToWebResponse(user, webResponseObj)
		return webResponse, err

	}

	if user.Password != req.GetOldPassword() {

		webResponseObj := web.WebResponse{
			Code:    400,
			Status:  false,
			Message: "wrong old password",
		}

		webResponse := domain.ConvertRequestToWebResponse(user, webResponseObj)
		return webResponse, err
	}

	if req.GetPassword() != req.GetConfirmPass() {

		webResponseObj := web.WebResponse{
			Code:    400,
			Status:  false,
			Message: "password doesn't match",
		}

		webResponse := domain.ConvertRequestToWebResponse(user, webResponseObj)
		return webResponse, err
	}

	user.Password = req.ConfirmPass //! change the existing password

	if err := ps.profileRepo.UpdateProfileByIDTx(ctx, user, true); err != nil {

		webResponseObj := web.WebResponse{
			Code:    400,
			Status:  false,
			Message: err.Error(),
		}

		webResponse := domain.ConvertRequestToWebResponse(domain.User{}, webResponseObj)
		return webResponse, status.Error(codes.NotFound, webResponse.Message)
	}

	webResponseObj := web.WebResponse{
		Code:    200,
		Status:  true,
		Message: "success to update password by user ID",
	}

	webResponse := domain.ConvertRequestToWebResponse(domain.User{}, webResponseObj)
	return webResponse, nil

}
