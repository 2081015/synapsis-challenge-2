1. generate proto file:
	protoc --go_out=./pb/ ./proto/*.proto

2. generate proto with GRPC's buffer file:
	protoc --proto_path=proto proto/*.proto --go_out=./proto --go-grpc_out=./proto

3. clean
    rm/pb*.go


how to install golang migrate in windows (powershell):
1. install scoop : iwr -useb get.scoop.sh | iex
2. solve err scoop download : Set-ExecutionPolicy RemoteSigned -scope CurrentUser
3. do again point 1
4. install go-migrate(powershell) : scoop install migrate


do migrate sql syntax to local db(postgresql): 
    migrate -database postgres://edwin:edwinSuek@localhost:5432/profilemanagement?sslmode=disable -path db/migrations up


vault API is :
"...an identity-based secrets and encryption management system. It allows you to secure, store and tightly control access to tokens,
 passwords, certificates, encryption keys for protecting secrets and other sensitive data using a UI, CLI, or HTTP API."